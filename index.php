<?php
require('vendor/autoload.php');
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;

try
{
    $time_start = microtime(true);
    $pdo = new PDO('mysql:host=localhost:3307;dbname=zad_dom', 'root', 'usbw');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "SELECT count(*) FROM items";
    $result = $pdo->prepare($sql);
    $result->execute();
    $number_of_rows = $result->fetchColumn();
    $limit = 10000;
    $offset = 0;
    $writer = WriterFactory::create(Type::XLSX);
    $writer->openToFile('export.xlsx');
    for($offset = 0; $offset<=$number_of_rows; $offset += $limit){
        $sql = $pdo->prepare('SELECT * FROM items LIMIT '.$limit.' OFFSET '.$offset);
        $sql->execute();
        $result = $sql->fetchAll();
        foreach($result as $row){
            $writer->addRow($row);
        }
    }

    $writer->close();
    $time_end = microtime(true);
    $execution_time = ($time_end - $time_start);
    echo '<b>Total Execution Time:</b> '.$execution_time;
}
catch(PDOException $e)
{
    echo 'Połączenie nie mogło zostać utworzone: ' . $e->getMessage();
} catch (\Box\Spout\Common\Exception\UnsupportedTypeException $e) {
} catch (\Box\Spout\Common\Exception\IOException $e) {
} catch (\Box\Spout\Writer\Exception\WriterNotOpenedException $e) {
}
